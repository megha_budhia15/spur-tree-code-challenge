Language used: Javascript

Testing
----------------------
1. Open any browser (Suggested: Google chrome)  
2. Press (ctrl+shift+i) key to open console (Or right click->inspect) 
3  Paste code from 'Traffic1.js' in the browser
4. Call calculateMinTime() function to get the output
  The function accepts 2 input parametrs
  1. A string with value of weather
  2. An array with traffic speed of Orbit1 and Orbit2 in order


Test Case1
-------------
calculateMinTime("Sunny", [12, 10])

Test Case2
-------------
calculateMinTime("Windy", [14, 20])


Output1
----------
Vehicle Tutuk on Orbit1

Output2
----------
Vehicle Car on Orbit2

5. Call calculateMinTime function with different set of parameters to test all cases

Assumption:
-----------
Time to cross each crater is very less than traffic speed

