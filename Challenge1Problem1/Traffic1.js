//speed in megamiles/hour
const vehicles = [
	{name: "Bike", speed:10, minsPerCrater: 2, weather: ['Sunny', 'Windy']},
	{name: "Tutuk", speed:12, minsPerCrater: 1, weather: ['Sunny', 'Rainy']},
	{name: "Car", speed:20, minsPerCrater: 3, weather: ['Sunny', 'Windy', 'Rainy']}
]

// The value here is % increase of craters
const weather ={Sunny: -10, Windy: 0, Rainy: 20 } 

//distance in megamiles
const routes = [{name:"Orbit1", noOfCraters: 20, distance: 18}, {name:"Orbit2", noOfCraters: 10, distance: 20}]

var calcTimeTakenToCrossCrater= (index, noOfCrater) => {
	return (noOfCrater*vehicles[index].minsPerCrater/60)
}

var validateInput = (currentWeather, trafficSpeed) => {
	if(weather[currentWeather]==null){
		console.log("Invalid Input: Weather!")
		return false
	}
	if( Object.prototype.toString.call( trafficSpeed ) !== '[object Array]' || trafficSpeed.length!=routes.length){

		console.log("Invalid input traffic speed! It Should be array of length: ", routes.length)
		return false
	}
	var flag = true
	trafficSpeed.map((value)=> {
		if(isNaN(value)){
			console.log("Traffic speed should be number!")
			flag= false
		}
	})
	if(!flag)
			return false
	return true
}

var clacNoOfCraters = (currentWeather, trafficSpeed) => {
	var cratersIncreasedPer =  weather[currentWeather]
	var routeState = [] 
	routes.map((route, index) => {
		var currentState ={}
		currentState.noOfCraters = route.noOfCraters*(100+ cratersIncreasedPer)/100
		currentState.trafficSpeed = trafficSpeed[index]
		routeState.push(currentState)
	})
	return routeState
}

var calcTimeForvehicle = (vehicleIndex, currentWeather, currentRoute, routeIndex) => {
	var vehicle = vehicles[vehicleIndex]
	var route = routes[routeIndex]
	var speed = Math.min(vehicle.speed, currentRoute.trafficSpeed)
	if(speed>0 && vehicle.weather.indexOf(currentWeather)>-1){
		var time = (route.distance/speed) + calcTimeTakenToCrossCrater(vehicleIndex, currentRoute.noOfCraters)
	}
	return time
}

//trafficSpeed is Array
var calculateMinTime = (currentWeather, trafficSpeed) => {
	if(validateInput(currentWeather, trafficSpeed)){
		var minTime = ""
		var result = {vehicleIndex: "", routeIndex: ""}
		var routeState = clacNoOfCraters(currentWeather, trafficSpeed)
		vehicles.map((vehicle, i) => {
			routeState.map((route, j) => {
				var time = calcTimeForvehicle(i, currentWeather, route, j )
				//console.log("---------", time)
				if(time && (!minTime || minTime>time  || (minTime==time && result.vehicleIndex>i) )){
					minTime= time
					result.vehicleIndex= i
					result.routeIndex= j 
				}
			})
		})
		if(minTime)
			console.log("Vehicle "+ vehicles[result.vehicleIndex].name + " on "+ routes[result.routeIndex].name)
		else
			console.log("Mission Impossible!")
	}
}




//Assuming time to cross each crater is very less than traffic speed