Language used: Javascript

Testing
----------------------
1. Open any browser (Suggested: Google chrome)  
2. Press (ctrl+shift+i) key to open console (Or right click->inspect) 
3. Paste code from 'Traffic2.js' in the browser
4. Call calculateMinTime() function to get the output
  The function accepts 2 input parametrs
  1. A string with value of weather
  2. An array of numbers with traffic speed of Orbit1, Orbit2, orbit3 and orbit4 in order


Note- (Refresh the browser before pasting new code for next prblem statement)


Test Case1
----------
calculateMinTime('Sunny', [20,12,15,12])

Test Case2
----------
calculateMinTime('Windy', [5,10,20,20])

Output1
----------
Vehicle Tutuk to Hallitharam via Orbit1 and RK Puram via Orbit4

Output2
----------
Vehicle Car to R K Puram via Orbit3 and Hallitharam via Orbit4

5. Call calculateMinTime function with different set of parameters to test all cases

Assumption:
-----------
1. Time to cross each crater is very less than traffic speed
2. King Shan will not travel  back on same route again
