//speed in megamiles/hour
const vehicles = [
	{name: "Bike", speed:10, minsPerCrater: 2, weather: ['Sunny', 'Windy']},
	{name: "Tutuk", speed:12, minsPerCrater: 1, weather: ['Sunny', 'Rainy']},
	{name: "Car", speed:20, minsPerCrater: 3, weather: ['Sunny', 'Windy', 'Rainy']}
]

// Value for each key here is % increase of craters
const weather ={Sunny: -10, Windy: 0, Rainy: 20 } 

const places= ['Silk Dorb', 'Hallitharam', 'RK Puram']

//distance in megamiles
const routes = [
	{name:"Orbit1", noOfCraters: 20, distance: 18, placesLinked: [0,1]}, 
	{name:"Orbit2", noOfCraters: 10, distance: 20, placesLinked: [0,1]},
	{name:"Orbit3", noOfCraters: 15, distance: 30, placesLinked: [0,2]},
	{name:"Orbit4", noOfCraters: 18, distance: 15, placesLinked: [1,2]}
]


var calcTimeTakenToCrossCrater= (index, noOfCrater) => {
	return (noOfCrater*vehicles[index].minsPerCrater/60)
}

var validateInput = (currentWeather, trafficSpeed) => {
	if(weather[currentWeather]==null){
		console.log("Invalid Input: Weather!")
		return false
	}
	if( Object.prototype.toString.call( trafficSpeed ) !== '[object Array]' || trafficSpeed.length!=routes.length){

		console.log("Invalid input traffic speed! It Should be array of length: ", routes.length)
		return false
	}
	var flag = true
	trafficSpeed.map((value)=> {
		if(isNaN(value)){
			console.log("Traffic speed should be number!")
			flag= false
		}
	})
	if(!flag)
			return false
	return true
}

var clacNoOfCraters = (currentWeather, trafficSpeed) => {
	var cratersIncreasedPer =  weather[currentWeather]
	var routeState = [] 
	routes.map((route, index) => {
		var currentState ={}
		currentState.noOfCraters = route.noOfCraters*(100+ cratersIncreasedPer)/100
		currentState.trafficSpeed = trafficSpeed[index]
		routeState.push(currentState)
	})
	return routeState
}

var calcTimeForvehicle = (vehicleIndex, currentWeather, currentRoute, routeIndex) => {
	var vehicle = vehicles[vehicleIndex]
	var route = routes[routeIndex]
	var speed = Math.min(vehicle.speed, currentRoute.trafficSpeed)
	if(speed>0 && vehicle.weather.indexOf(currentWeather)>-1){
		var time = (route.distance/speed) + calcTimeTakenToCrossCrater(vehicleIndex, currentRoute.noOfCraters)
	}
	return time
}

//trafficSpeed is Array
var calculateMinTime = (currentWeather, trafficSpeed) => {
	if(validateInput(currentWeather, trafficSpeed)){
		var minTime = ""
		var result = {vehicleIndex: "", completeRouteIndices: ""}
		var routeState = clacNoOfCraters(currentWeather, trafficSpeed)

		vehicles.map((vehicle, i) => {
			var timeTaken = []
			routeState.map((route, j) => {
				var time = calcTimeForvehicle(i, currentWeather, route, j )
				//console.log("---------", time)
				timeTaken.push(time)
			})
			var output = checkRouteWithMinTime(timeTaken)
			var totalTime= output.time

			if(totalTime && (!minTime || minTime>totalTime  || (minTime==totalTime && result.vehicleIndex>i) )){
					minTime= totalTime
					result.vehicleIndex= i
					result.completeRouteIndices=  output.completeRouteIndices
			}
		})

		var completeRouteIndex = result.completeRouteIndices
		var completePlacesIndex= []
		var startPlace = 0
		var endPlace=""
		completeRouteIndex.map((routeIndex) => {
			var routePlaces= routes[routeIndex].placesLinked
			endPlace= routePlaces[0]==startPlace? routePlaces[1] : routePlaces[0] 
			completePlacesIndex.push(endPlace)
			startPlace= endPlace //end place for thos route. start place for next route
			console.log("--startPlace---", startPlace)
		})		
		if(minTime)
			console.log("Vehicle "+ vehicles[result.vehicleIndex].name + " to "+ 
				places[completePlacesIndex[0]] + " via " + routes[completeRouteIndex[0]].name + " and " +
				places[completePlacesIndex[1]] + " via " + routes[completeRouteIndex[1]].name
			)
	}
}

var checkRouteWithMinTime = (times) => {
	//possibele routes:
	//Only route that connects Hallitharam and R K Puram is Orbit 4; route= (R1/R2/R3) + R4

	var shortestRoute=""
	//Calculating shortest time taken in which route R1, R2 or R3
	if(times[0]<times[1] && times[0]<times[2])
		shortestRoute=0
	else if(times[1]> times[2])
		shortestRoute=2
	else
		shortestRoute=1
	var totalTimeTakenByVehicle= times[shortestRoute]+ times[3]
	//console.log("================", totalTimeTakenByVehicle, shortestRoute )
	var completeRouteIndices= [shortestRoute,3]
	return({time: totalTimeTakenByVehicle, completeRouteIndices: completeRouteIndices})
}



